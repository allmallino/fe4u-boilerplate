require('../css/app.css');
import { countries } from './countries.js';
import { importUsers, addUsers } from './requests.js';
import { randomInt, getFiltredUsers, findUsers, getSorted, validateUser } from './helpers.js';

async function getUsers(count = 50) {
    users_all = users_all.concat(await importUsers(count));
    renderUsers(true);
}

function createTableLi(cf, options) {
    let li = document.createElement('li');
    let a = document.createElement('a');
    a.href = "#statistics";
    a.className = options.cl;
    a.text = options.txt;
    a.onclick = cf;
    li.append(a);
    return li;
}

function createUserElement(user) {
    let figure = document.createElement('figure');
    figure.className = "favorite-img";

    let frame = document.createElement('div');
    frame.className = "main-image-conteiner";

    frame.append(getImageCover(user));
    figure.append(frame);

    let name = document.createElement('p');
    name.className = "name";
    name.textContent = user.full_name;
    figure.append(name);
    let region = document.createElement('p');
    region.className = "region";
    region.textContent = user.country;
    figure.append(region);
    return figure;
}

function getImageCover(user) {
    let image = document.createElement('img');
    image.className = "mainimage";
    image.onclick = function () {
        let info = document.querySelectorAll(".info");
        document.getElementById('teacher-view').style.visibility = 'visible';
        info[0].src = user.picture_large;
        info[1].src = user.favorite ? "images/Star-filled.png" : "images/Star.png";
        info[1].onclick = function () {
            user.favorite = !user.favorite;
            this.src = user.favorite ? "images/Star-filled.png" : "images/Star.png";
            updateFavorites(getFiltredUsers(users_all, "", function (user) { return true; }, "", true));
            renderUsers(true);
        };
        info[2].textContent = user.full_name;
        info[3].textContent = user.course;
        info[4].textContent = user.city + " , " + user.country;
        info[5].textContent = user.age + ", " + user.gender;
        info[6].textContent = user.email;
        info[6].href = "mailto:" + user.email;
        info[7].textContent = user.phone;
        info[8].textContent = user.note;
    };
    image.src = user.picture_large;
    let text = "";
    for (let t of user.full_name.split(" ")) {
        text += t[0] + ".";
    }
    image.alt = text;
    return image;
}

function updateContent(users) {
    let imageList = document.getElementById("imagelist");
    imageList.innerHTML = "";
    let fragmet = document.createDocumentFragment();
    for (let i = 0; i < users.length; i++) {
        let user = users[i];
        let figure = document.createElement('figure');
        let star = document.createElement('img');
        star.className = "favor-sign";
        star.src = "images/Star-filled.png";
        star.alt = "Star";
        figure.append(star);

        let frame = document.createElement('div');
        frame.className = "main-image-conteiner";

        frame.append(getImageCover(user));
        figure.append(frame);
        let name = document.createElement('p');
        name.className = "name";
        name.textContent = user.full_name;
        figure.append(name);
        let subject = document.createElement('p');
        subject.className = "subject";
        subject.textContent = user.course;
        figure.append(subject);
        let region = document.createElement('p');
        region.className = "region";
        region.textContent = user.country;
        figure.append(region);
        fragmet.append(figure);
    }
    imageList.append(fragmet);
}

function updateFavorites(users, currentIndex = 0) {
    let imageList = document.getElementById("imagelist-favorites");
    imageList.innerHTML = "";
    let fragmet = document.createDocumentFragment();
    if (users.length <= 5) {
        let div = document.createElement("div");
        div.className = "imagelist";
        div.style.margin = "0 100px";

        for (let i = 0; i < users.length; i++) {
            div.append(createUserElement(users[i]));
        }
        fragmet.append(div);
        imageList.append(fragmet);
        return;
    }

    let count = Math.ceil(users.length / 5);
    let arrow = document.createElement("img");
    arrow.className = "sign";
    arrow.onclick = function () {
        updateFavorites(users, (currentIndex - 1 + count) % count);
    }
    fragmet.append(arrow);

    let div = document.createElement("div");
    div.className = "imagelist";

    for (let i = 0; i < 5; i++) {
        div.append(createUserElement(users[(currentIndex * 5 + i) % users.length]));
    }
    fragmet.append(div);

    arrow = document.createElement("img");
    arrow.className = "sign";
    arrow.onclick = function () {
        updateFavorites(users, (currentIndex + 1) % count);
    }
    fragmet.append(arrow);
    imageList.append(fragmet);
}

function filterUsers() {
    let selects = document.querySelectorAll(".filter-input");
    let search = document.getElementById("search");

    let func;
    switch (selects[0].value.toString()) {
        case "1":
            func = function (user) { return user.age <= 31; }
            break;
        case "2":
            func = function (user) { return user.age > 31 && user.age <= 45; }
            break;
        case "3":
            func = function (user) { return user.age > 45 && user.age <= 59; }
            break;
        case "4":
            func = function (user) { return user.age > 60; }
            break;
        default:
            func = function (user) { return true; }
            break;
    }
    let fusers = getFiltredUsers(users_all, selects[1].value, func, selects[2].value, selects[4].checked);
    return findUsers(fusers, search.value);

}

function updateTable(users, index) {
    let table = document.getElementById("table");
    let heading = table.firstChild;
    for (let head of heading.firstChild.children) {
        head.onclick = function () {
            switch (this.className) {
                case "sorted_false":
                    this.className = "sorted_true";
                    updateTable(getSorted(users, this.firstChild.dataset.value, true), 1);
                    break;
                case "sorted_true":
                    this.className = "";
                    updateTable(filterUsers(), 1);
                    break;
                default:
                    for (let h of heading.firstChild.children) {
                        h.className = "";
                    }
                    this.className = "sorted_false";
                    updateTable(getSorted(users, this.firstChild.dataset.value), 1);
                    break;
            }
        }
    }
    table.innerHTML = "";
    table.append(heading);
    let fragmet = document.createDocumentFragment();
    for (let i = (index - 1) * 10; i < users.length && i - (index - 1) * 10 < 10; i++) {
        let tr = document.createElement('tr');
        let name = document.createElement('td');
        name.textContent = users[i].full_name;
        tr.append(name);
        let course = document.createElement('td');
        course.textContent = users[i].course;
        tr.append(course);
        let age = document.createElement('td');
        age.textContent = users[i].age;
        tr.append(age);
        let gender = document.createElement('td');
        gender.textContent = users[i].gender;
        tr.append(gender);
        let country = document.createElement('td');
        country.textContent = users[i].country;
        tr.append(country);
        fragmet.append(tr)
    }
    table.append(fragmet);
    createTableLinks(users, index);

}

function createTableLinks(users, index) {
    let tableLinks = document.getElementById("table-links");
    tableLinks.innerHTML = "";
    let fragmet = document.createDocumentFragment();
    let count = Math.ceil(users.length / 10);
    if (count > 3) {
        if (index > 2) {
            fragmet.append(createTableLi(() => updateTable(users, 1), { cl: "text-interface", txt: "1" }));

            let points = document.createElement('li');
            points.textContent = "...";
            fragmet.append(points);

            for (let i = -1; i <= 1 && i + index <= count; i++) {
                fragmet.append(createTableLi(() => updateTable(users, i + index), { cl: i === 0 ? "text-interface selected" : "text-interface", txt: i + index }));
            }
            if (index == count || index + 1 == count) {
                tableLinks.append(fragmet);
                return;
            }
            points = document.createElement('li');
            points.textContent = "...";
            fragmet.append(points);

            fragmet.append(createTableLi(() => updateTable(users, count), { cl: "text-interface", txt: "Last" }));

        } else {
            for (let i = 1; i <= 3; i++) {
                fragmet.append(createTableLi(() => updateTable(users, i), { cl: i === index ? "text-interface selected" : "text-interface", txt: i }));
            }
            let points = document.createElement('li');
            points.textContent = "...";
            fragmet.append(points);
            fragmet.append(createTableLi(() => updateTable(users, count), { cl: "text-interface", txt: "Last" }));

        }
    } else {
        for (let i = 1; i <= count; i++) {
            fragmet.append(createTableLi(() => updateTable(users, i), { cl: i === index ? "text-interface selected" : "text-interface", txt: i }));
        }
    }
    tableLinks.append(fragmet);
}

function renderUsers(renderFavorites = false) {
    updateContent(filterUsers());
    let heading = document.getElementById("table").firstChild;
    for (let h of heading.firstChild.children) {
        h.className = "";
    }
    updateTable(filterUsers(), 1);
    if (renderFavorites) {
        updateFavorites(getFiltredUsers(users_all, "", function (user) { return true; }, "", true));
    }
}

function addUser() {
    let inputs = document.querySelectorAll(".input");
    let gend = document.getElementsByName("gender");
    let note = document.querySelector("textarea");

    let user = {
        "id": "" + randomInt(999999, 1),
        "gender": gend[0].checked ? gend[0].value : gend[1].checked ? gend[1].value : null,
        "title": "",
        "full_name": inputs[0].value,
        "city": inputs[3].value,
        "country": inputs[2].value,
        "postcode": "",
        "coordinates": {},
        "timezone": {},
        "email": inputs[4].value,
        "b_date": inputs[6].value,
        "age": Math.floor((Date.now() - Date.parse(inputs[6].value)) / 31536000000),
        "phone": inputs[5].value,
        "picture_large": "",
        "picture_thumbnail": "",
        "favorite": false,
        "course": inputs[1].value,
        "bg_color": inputs[7].value,
        "note": note.value
    }

    if (validateUser(user)) {
        addUsers(user);
        users_all.push(user);
        renderUsers();
        document.getElementById("form").reset();
        note.value = "";
        document.getElementById('teacher-add-menu').style.visibility = 'hidden';
    }

}

var users_all = [];
getUsers();
let selects = document.querySelectorAll(".filter-input");
let region = selects[1];
let sortregion = getSorted(countries, "code");
for (let i = 0; i < sortregion.length; i++) {
    let country = new Option(sortregion[i].code, sortregion[i].name);
    region.options.add(country);
}

for (let s of selects) {
    s.onchange = renderUsers;
}
let search = document.getElementById("search-btn");
search.onclick = renderUsers;
let country_input = document.getElementsByName("country")[0];
sortregion = getSorted(countries, "name");
for (let i = 0; i < sortregion.length; i++) {
    let country = new Option(sortregion[i].name);
    country_input.options.add(country);
}
document.getElementById("form").addEventListener("submit", function (event) {
    event.preventDefault();
    addUser();
});
document.querySelectorAll(".new-users")[0].onclick = () => { getUsers(10) };
