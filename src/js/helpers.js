import isValidPhoneNumber from '../../node_modules/libphonenumber-js/index';
import { countries } from './countries.js';

export function randomInt(max, min = 0) {
    return Math.floor(Math.random() * (max - min)) + min;
}

export function getSorted(array, column_name = "", DowntoUp = false) {
    let narray = array.concat();

    narray.sort(function (a, b) {
        if (a[column_name] == undefined && b[column_name] == undefined) {
            return 0;
        }
        if (a[column_name] == undefined) {
            return 1;
        }
        if (b[column_name] == undefined) {
            return -1;
        }

        if (typeof (a[column_name]) == "string") {
            let x = a[column_name].toLowerCase();
            let y = b[column_name].toLowerCase();

            if (x < y) { return -1; }
            if (x > y) { return 1; }
            return 0;
        }
        return a[column_name] - b[column_name];
    })

    if (DowntoUp) {
        narray.reverse();
    }

    return narray;
}

export function validateUser(user) {

    if (user.full_name === "") {
        alert("Name isn't correct");
        return false;
    } else if (user.full_name[0] !== user.full_name[0].toUpperCase()) {
        alert("Name isn't correct");
        return false;
    }

    if (user.course === "") {
        alert("Speciality isn't correct");
        return false;
    }

    if (user.country === "") {
        alert("Country isn't correct");
        return false;
    }

    if (user.gender === "") {
        alert("Gender isn't correct");
        return false;
    }

    if (user.city === "") {
        alert("City isn't correct");
        return false;
    } else if (user.city[0] !== user.city[0].toUpperCase()) {
        alert("City isn't correct");
        return false;
    }

    if (!user.email.includes("@")) {
        alert("Email isn't correct");
        return false;
    }

    if (!isValidPhoneNumber(user.phone, countries.find(countries => countries.name === user.country).code)) {
        alert("Phone number isn't correct");
        return false;
    }

    if (Number.isNaN(user.age) || user.age < 18) {
        alert("Age isn't correct");
        return false;
    }

    if (user.gender == undefined) {
        alert("Gender isn't correct");
        return false;
    }

    alert(user.full_name + " is added");
    return true;
}

export function getFormattedUser(userMock) {
    let courses = ["Mathematics", "Physics", "English", "Computer Science", "Dancing", "Chess", "Biology", "Chemistry", "Law", "Art", "Medicine", "Statistics"];
    const users = [];

    for (let i = 0; i < userMock.length; i++) {
        let user = userMock[i];

        let formattedUser = {
            "gender": user.gender,
            "title": user.name.title,
            "full_name": user.name.first + " " + user.name.last,
            "city": user.location.city,
            "country": user.location.country,
            "postcode": user.location.postcode,
            "coordinates": user.location.coordinates,
            "timezone": user.location.timezone,
            "email": user.email,
            "b_date": user.dob.date,
            "age": user.dob.age,
            "phone": user.phone,
            "picture_large": user.picture.large,
            "picture_thumbnail": user.picture.thumbnail,
            "id": user.id.name + user.id.value,
            "favorite": !!randomInt(2),
            "course": courses[randomInt(courses.length - 1)],
            "bg_color": "#" + Math.floor(Math.random() * 16777215).toString(16),
            "note": ""
        }

        users.push(formattedUser);
    }

    return users;
}

export function findUsers(users, search) {

    return users.filter((user) => { return user.full_name.toLowerCase().includes(search.toLowerCase()) || user.note.toLowerCase().includes(search.toLowerCase()) || user.age == search; })

}

export function getFiltredUsers(users, country, age, gender, favorite) {

    let nusers = users.filter((user) => { return user.country.toLowerCase().includes(country.toLowerCase()) && age(user) && user.gender.startsWith(gender); })
    return favorite ? nusers.filter((user) => { return user.favorite === true; }) : nusers;
}