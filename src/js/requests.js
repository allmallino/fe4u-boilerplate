import { getFormattedUser } from './helpers.js';

function http(url, method, value = null) {
    return new Promise((resolve, reject) => {

        let xhr = new XMLHttpRequest();
        xhr.open(method, url, true);

        xhr.setRequestHeader('Content-Type', 'application/json');

        xhr.onload = function () {
            if (this.status === 200 || this.status === 201) {
                resolve(this.response);
            } else {
                let error = new Error(this.statusText);
                error.code = this.status;
                reject(error);
            }
        }

        xhr.onerror = function () {
            reject(new Error("Network Error"));
        }

        xhr.send(value);
    });
}


export async function importUsers(count = 1) {
    let response = await http("https://randomuser.me/api?results=" + count, 'GET');
    let users = getFormattedUser(JSON.parse(response).results);
    return users;
}

export async function addUsers(user) {
    let response = await http("http://localhost:3000/users", 'POST', JSON.stringify(user));
    console.log(response);
}